"use strict"

// 1. Живой поиск
// 2. Запрет английских символов и первого пробела
// 3. Блок с сообщением об отсутствии результатов поиска
// 4. Удаление результатов поиска
// 5. Вывод количества совпадений

$(document).ready(function () {

    var departments = $('.b-department'); //собираем все отделы    

    var $searchValue = $('#searchValue'); //зацепляемся за строку поиска

    $searchValue.on('input', function () {

        var $departments = departments.clone(); //делаем копию массива с отделами

        var search = $searchValue.val();

        if (!search) {
            $('.search').remove(); //класс .search не несет с собой никаких свойств - нужен только для манипуляций с jQuery
        }

        if ($('.blockSearchNull')) {
            $('.blockSearchNull').remove();
        }

        if (banSymbol(search)) {
            $('.b-office').hide();  //скрываем оригинальный блок контактов 
            $('.all-full').hide();  //скрываем кнопки
            $('.all-null').hide();
            $('#searchBack').show(); //показываем кнопку очистки строки поиска
            $('#concurrences').show(); // счетчик совпадений
            $('.search').remove();
            $('.b-contacts').append($departments);
            $departments.addClass('height-full search');
            var searchRequest = new RegExp('(' + search + ')', 'gi');

            $departments.each(function () {
                var $employees = $(this).children('.b-employees');

                $employees.each(function () {
                    var $tr = $(this).find('tr');
                    $tr.each(function () {
                        var $text = $(this).text();
                        if (!searchRequest.test($text)) {
                            $(this).remove();
                        } else {
                            //$(this).show();
                            $(this).html($(this).html().replace(searchRequest, '<mark>$1</mark>'));
                        }
                    })
                });
            })

            $departments.each(function () {
                if ($(this).find('tr').length === 0) {
                    $(this).remove();
                }
            });
            $('#concurrences-num').text($('.search').length);
            var $countConcurrences = $('.search').length;            
            $('.b-wrapper').css('overflow-y', 'auto');            
            if ($countConcurrences === 0) {
                var $blokSearchNull = $('<div class="blockSearchNull"/>').html('Совпадений не найдено! Попробуйте ввести другое значение или очистить строку поиска, нажав кнопку <img src="css/img/searchBack.png" width="16" height="16">');                
                $('.b-wrapper').append($blokSearchNull)
            }
        } else {
            $('.all-full').show();
            $('.all-null').show();
            $('.b-office').show();
            $('#searchBack').hide();
            $('#concurrences').hide();
            //$('.b-department').not(':hidden').remove();
            //$('.b-office').show();  //показываем оригинальный блок контактов    
        }

        //$departments.on('click', function () {
        //    $(this).toggleClass('height-full')
        //});

    });

    $('#searchBack').click(function () { //Кнопка очистки строки поиска
        $('#searchValue').val('');
        $('.all-full').show();
        $('.all-null').show();
        $('.b-office').show();
        $('#searchBack').hide();
        $('#concurrences').hide();
        $('.search').remove();
        $('.b-wrapper').css('overflow-y', 'scroll');            
    });

    function banSymbol(search) {

        // получить поисковую строку и разбить ее в массив
        var banMessage = document.getElementById('ban-message');
        var banSymbol = search.split('');
        //найти в ней английские символы
        var banSymbolTemplate = /[A-Za-z_<>,.@:;"'=\/\[\]]/g; // шаблон запрещенных символов    
        // получить позицию английского символа
        for (var i = 0; i < banSymbol.length; /*шаг итерации задается только если символ не из запрещенных*/) {                  // обход всех символов        
    
            if (banSymbol[i].search(banSymbolTemplate) !== -1 || banSymbol[0].search(/\s/) === 0) { //если есть совпадения            
                banSymbol.splice(i, 1);            // удалить из массива этот символ
                banMessage.style.display = 'block';
                setTimeout(
                    function () {
                        banMessage.style.display = 'none';
                    }
                    , 5000
                )                
            } else {
                i++;  // в противном случае, увеличиваем индекс на 1 и проверяем следующий символ
                banMessage.style.display = 'none';
            }
        }
    
        document.getElementById('searchValue').value = banSymbol.join(''); // меняем значение в поисковой строке
        search = banSymbol.join(''); // склеиваем все символы в поисковый запрос, для передачи в функцию поиска
        
        return search; 
       
    }   

});



