"use strict"

$(document).ready(function () {

    // Добавить задачу ----------------------------

    function addTask() {
        var $taskText = $('#todo-new-text').val();
        if ($taskText !== '' || $taskText.length > 0) {
            $('#todo-new-text').css('box-shadow', 'inset 0 0 0 2px #90c768');
            var todoItem = '<li class="task"><input type="checkbox" class="todo-checkbox"><span>' + $taskText + '</span><input class="todo-remove-button" type="button" value="&#10006;"></li>'
            $('#todo-list ul').append(todoItem);
            $('#todo-new-text').val('');
            writeKeyValue();
            message();
        } else {
            $('#todo-new-text').css('box-shadow', 'inset 0 0 0 2px red');
        };
    }

    // Удалить задачу ---------------------------------

    function removeTask() {
        //console.log($(this).closest('.task').html());
        $(this).closest('.task').remove();
        message();

        var count = $('.task').length;
        console.log(count);

        if ($('.task').length > 0) {
            writeKeyValue();
        } else {
            localStorage.setItem("todo_list-items", JSON.stringify(false));
        }


    }

    // Управление состоянием задачи --------------------

    function stateTask(state, jQuery) {

        var $text = jQuery.next('span');

        if (state) {
            $text.css({
                'color': '#999',
                'text-decoration': 'line-through'
            });
        } else {
            $text.css({
                'color': '#90c768',
                'text-decoration': 'none'
            });
        }
    }

    readKeyValue();
    //writeKeyValue();

    // Событие при щелчке на чек-бокс

    $('#task-list').on('change', '.todo-checkbox', function () {
        var $state = $(this).prop('checked');
        stateTask($state, $(this));
        writeKeyValue();
    });

    // Добавление задачи по клику или нажатии Enter

    $('#todo-new-button').on('click', addTask);
    $('#todo-new-text').keypress(function (e) {
        if (e.which == 13) {
            addTask();
            writeKeyValue();
        }
    });

    // Удаление задачи

    $('#task-list').on('click', '.todo-remove-button', removeTask);

    // Смена рамки поля с красного на зеленый при появление символа

    $('#todo-new-text').keyup(function () {
        if ($('#todo-new-text').val()) {
            $('#todo-new-text').css('box-shadow', 'inset 0 0 0 2px #90c768');
        }
    });

    // Включение Todo-list

    var todo = JSON.parse(localStorage.getItem('todo_list'));
    //console.log(typeof todo, todo);

    if (!todo) {
        var todo = {};

        todo.show = false;

        localStorage.setItem("todo_list", JSON.stringify(todo));
        //console.log(localStorage.getItem("todo_list"));
    }

    if (todo.show) {
        //console.log(typeof localStorage.getItem('todo_list'), 'флаг тру')
        $('#todo-list').css('right', 0);
    } else {
        console.log('флаг фолсе')
        $('#todo-list').css('right', -290);
    }

    // Тестовая кнопка, добавляющая Todo-list

    $('.todo-list_state').click(function () {

        $(this).toggleClass('active')

        var todoList = {};

        if ($(this).hasClass('active')) {
            
            $(this).val('▶');

            todoList.show = true;

            localStorage.setItem("todo_list", JSON.stringify(todoList));

            var todo_list = JSON.parse(localStorage.getItem('todo_list'));
            console.log(typeof todo_list, todo_list);

            if (todo_list.show) {
                $('#todo-list').animate({ 'right': 0 });
            }
        } else {           

            $(this).val('◀');

            todoList.show = false;

            localStorage.setItem("todo_list", JSON.stringify(todoList));

            var todo_list = JSON.parse(localStorage.getItem('todo_list'));
            console.log(typeof todo_list, todo_list);

            if (!todo_list.show) {
                $('#todo-list').animate({ 'right': -290 });
            }
        }
    });

    //// Тестовая кнопка, удаляющая Todo-list
//
    //$('#test1').click(function () {
//
//
    //})

    // Запись в Локалсторидж текущего состояния Todo-list 

    function writeKeyValue() {
        //var todoList = {};
        var todoListItems = [];


        $('.task').each(function () {
            var $checked = $(this).find('.todo-checkbox').prop('checked');
            var $value = $(this).find('span').text();

            var todoListItem = {
                checked: $checked,
                value: $value
            }

            todoListItems.push(todoListItem);

            if (todoListItems) {
                localStorage.setItem("todo_list-items", JSON.stringify(todoListItems));
            }
        });
    }

    // Считывание из Локалсторидж текущего состояния Todo-list

    function readKeyValue() {
        var taskList = JSON.parse(localStorage.getItem("todo_list-items"));
        //console.log(taskList);

        if (taskList) {
            renderTodoList(taskList);
        } else {
            message()
        }
    }

    function message() {
        if ($('.task').length > 0) {
            $('p.message').remove();
        } else {
            var msg = ($('<p>', {
                class: 'message',
                text: 'Добавьте новую задачу'
            }));

            $('#todo-list').append(msg);
            //console.log(msg);
        }
    }

    // рендер списка дел
    function renderTodoList(taskList) { //taskList - объект, содержащий в себе массив с объектами списка дел        

        var ul = $('#task-list');
        ul.children('li').remove();
        for (var i = 0; i < taskList.length; i++) {

            var li = $('<li>', { class: 'task' });
            ul.append(li);
            if (taskList[i].checked) {
                //console.log('Крыжик поставлен')
                li.append($('<input type="checkbox" class="todo-checkbox" checked="checked">'));
                li.append($('<span>' + taskList[i].value + '</span>'));
                li.append($('<input class="todo-remove-button" type="button" value="&#10006;">'));
            } else {
                //console.log('Крыжик не поставлен');
                li.append($('<input type="checkbox" class="todo-checkbox">'));
                li.append($('<span>' + taskList[i].value + '</span>'));
                li.append($('<input class="todo-remove-button" type="button" value="&#10006;">'));
            }
        }
    }
})




